fn main(){
    print!("Hello World\n"); //like printf in C it does not contain line break
    println!("Hello Mars"); //like printf in C but with line break
}
