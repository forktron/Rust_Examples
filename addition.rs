use std::io;

fn main()
{
    println!("Enter the value for A: ");
    
    let mut a = String::new();
    io::stdin().read_line(&mut a).expect("Enter valid number!");

    println!("Enter the value for B: ");
    let mut b = String::new();
    io::stdin().read_line(&mut b).expect("Enter valid number!");

    let a2 = a.trim().parse::<i32>().unwrap();
    let b2 = b.trim().parse::<i32>().unwrap();

    let c = a2 + b2;

    println!("Addition of {} and {} is: {}", a2, b2, c);
    
}
