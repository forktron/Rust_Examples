fn main()
{
    let x: i8 = 99;
    println!("The value for X is: {}", x);
    {
        let x: i8 = 30;
        println!("The value for X is: {}", x);
    }
    println!("The value for X is: {x}");
}
