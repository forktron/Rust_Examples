use std::io;

fn main()
{

    println!("Choose the Following Option:\n1) Celsius to Fahrenheit\n2) Fahrenheit to Celsius\nOption: ");

    let mut option = String::new();
    io::stdin().read_line(&mut option).expect("error: enter valid value!");

    let new_option = option.trim().parse::<i8>().unwrap();

    if new_option == 1 {
        println!("Enter Celsius: ");
        let mut celsius = String::new();
        io::stdin().read_line(&mut celsius).expect("Error: enter valid value!");

        let _celsius = celsius.trim().parse::<f32>().unwrap();

        let fahrenheit: f32 = (_celsius * 9.0/5.0)+32.0;

        println!("Celsius {} is for {} in Fahrenheit", _celsius, fahrenheit);
    }
    else if new_option == 2 {
        println!("Enter Fahrenheit: ");
        let mut fahrenheit = String::new();
        io::stdin().read_line(&mut fahrenheit).expect("Error: enter valid value!");

        let _fahrenheit = fahrenheit.trim().parse::<f32>().unwrap();

        let celsius: f32 = (_fahrenheit - 32.0)*5.0/9.0;

        println!("Fahrenheit {} is for {:.4} in Celsius", _fahrenheit, celsius);
    }
    else
    {
        println!("Error: Choose correct option!");
    }
}
