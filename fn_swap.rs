fn main()
{
    let (a, b) = (45, 56);

    println!("Before swap of A & B: {a}, {b}");

    swap(a, b);
}

fn swap(mut x: i8, mut y: i8)
{
    let mut _temp;

    _temp = x;
    x = y;
    y = _temp;

    println!("After swap of A & B is: {x}, {y}");
}
