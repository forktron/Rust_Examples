fn main()
{
    let a: bool = true;

    if a
    {
        println!("A is true!");
    }

    let b: bool = false;

    if b
    {
        println!("B is true!");
    }
    else
    {
        println!("B is false!");
    }

}
